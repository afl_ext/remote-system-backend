<?php
declare(strict_types=1);

namespace App\Entity;

use App\Form\UserEditData;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var int
     */
    private $id;

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username): void
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param mixed $apiKey
     */
    public function setApiKey($apiKey): void
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return mixed
     */
    public function getHomeLat()
    {
        return $this->homeLat;
    }

    /**
     * @param mixed $homeLat
     */
    public function setHomeLat($homeLat): void
    {
        $this->homeLat = $homeLat;
    }

    /**
     * @return mixed
     */
    public function getHomeLong()
    {
        return $this->homeLong;
    }

    /**
     * @param mixed $homeLong
     */
    public function setHomeLong($homeLong): void
    {
        $this->homeLong = $homeLong;
    }

    /**
     * @return mixed
     */
    public function getHomeRange()
    {
        return $this->homeRange;
    }

    /**
     * @param mixed $homeRange
     */
    public function setHomeRange($homeRange): void
    {
        $this->homeRange = $homeRange;
    }

    /**
     * @return mixed
     */
    public function getCurrentLat()
    {
        return $this->currentLat;
    }

    /**
     * @param mixed $currentLat
     */
    public function setCurrentLat($currentLat): void
    {
        $this->currentLat = $currentLat;
    }

    /**
     * @return mixed
     */
    public function getCurrentLong()
    {
        return $this->currentLong;
    }

    /**
     * @param mixed $currentLong
     */
    public function setCurrentLong($currentLong): void
    {
        $this->currentLong = $currentLong;
    }

    /**
     * @return mixed
     */
    public function getisSystemArmed()
    {
        return $this->isSystemArmed;
    }

    /**
     * @param mixed $isSystemArmed
     */
    public function setIsSystemArmed($isSystemArmed): void
    {
        $this->isSystemArmed = $isSystemArmed;
    }

    public function getDistanceToHome(): float
    {
        $lat1 = $this->currentLat;
        $lat2 = $this->homeLat;
        $lon1 = $this->currentLong;
        $lon2 = $this->homeLong;
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $meters = $dist * 60.0 * 1.1515 * 1.609344 * 1000.0;
        return $meters;
    }

    public function isInHomeRange(): bool
    {
        return $this->getDistanceToHome() <= $this->homeRange;
    }

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $apiKey;

    /**
     * @ORM\Column(type="float")
     */
    private $homeLat;

    /**
     * @ORM\Column(type="float")
     */
    private $homeLong;

    /**
     * @ORM\Column(type="float")
     */
    private $currentLat;

    /**
     * @ORM\Column(type="float")
     */
    private $currentLong;

    /**
     * @ORM\Column(type="float")
     */
    private $homeRange;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isSystemArmed;

}
