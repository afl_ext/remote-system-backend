<?php
declare(strict_types=1);

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationForm;
use App\Form\UserData;
use App\Form\UserEditData;
use App\Form\UserEditForm;
use App\Repository\UserRepository;
use App\Service\RegistrationTokenStorageInterface;
use App\Service\TokenSerializerInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;


class ApiController extends Controller
{
    private $params;

    private function extractUser(Request $request, UserRepository $userRepository): ?User
    {
        $params = [];
        $content = $request->getContent();
        if (!empty($content)) {
            $params = json_decode($content, true);
        }
        if (!array_key_exists("apiKey", $params)) {
            return null;
        }
        $apiKey = $params["apiKey"];
        $this->params = $params;
        return $userRepository->findByApiKey($apiKey);
    }

    /**
     * @Route("/validate-api-key", name="validate-api-key")
     * @param Request $request
     * @param UserRepository $userRepository
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function validateApiKey(Request $request, UserRepository $userRepository)
    {
        $user = $this->extractUser($request, $userRepository);

        $result = false;
        if (null !== $user) {
            $result = true;
        }
        return $this->json(['result' => $result]);
    }

    /**
     * @Route("/is-system-armed", name="is-system-armed")
     * @param Request $request
     * @param UserRepository $userRepository
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function isSystemArmed(Request $request, UserRepository $userRepository)
    {
        $user = $this->extractUser($request, $userRepository);
        if (null === $user) {
            return $this->json(['error' => 'invalid api key'], Response::HTTP_UNAUTHORIZED);
        }
        return $this->json(['result' => $user->getIsSystemArmed()]);
    }

    /**
     * @Route("/set-system-armed", name="set-system-armed")
     * @param Request $request
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function setSystemArmed(Request $request, UserRepository $userRepository, EntityManagerInterface $entityManager)
    {
        $user = $this->extractUser($request, $userRepository);
        if (null === $user) {
            return $this->json(['error' => 'invalid api key'], Response::HTTP_UNAUTHORIZED);
        }

        if (!array_key_exists("status", $this->params)) {
            return $this->json(['error' => 'status not provided'], Response::HTTP_BAD_REQUEST);
        }
        $newStatus = $this->params['status'];
        if (!is_bool($newStatus)) {
            return $this->json(['error' => 'status not boolean'], Response::HTTP_BAD_REQUEST);
        }
        $user->setIsSystemArmed($newStatus);
        $entityManager->merge($user);
        $entityManager->flush();

        return $this->json(['result' => $user->getIsSystemArmed()]);
    }

    /**
     * @Route("/update-location", name="update-location")
     * @param Request $request
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function updateLocation(Request $request, UserRepository $userRepository, EntityManagerInterface $entityManager)
    {
        $user = $this->extractUser($request, $userRepository);
        if (null === $user) {
            return $this->json(['error' => 'invalid api key'], Response::HTTP_UNAUTHORIZED);
        }

        if (!array_key_exists("lat", $this->params)) {
            return $this->json(['error' => 'latitude not provided'], Response::HTTP_BAD_REQUEST);
        }
        if (!array_key_exists("lon", $this->params)) {
            return $this->json(['error' => 'longitude not provided'], Response::HTTP_BAD_REQUEST);
        }

        $newLat = $this->params['lat'];
        $newLon = $this->params['lon'];

        if (!is_double($newLat)) {
            return $this->json(['error' => 'latitude not double'], Response::HTTP_BAD_REQUEST);
        }
        if (!is_double($newLon)) {
            return $this->json(['error' => 'longitude not double'], Response::HTTP_BAD_REQUEST);
        }

        $user->setCurrentLat($newLat);
        $user->setCurrentLong($newLon);
        $entityManager->merge($user);
        $entityManager->flush();

        return $this->json(['result' => $user->getDistanceToHome()]);
    }

    /**
     * @Route("/get-home-distance", name="get-home-distance")
     * @param Request $request
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getHomeDistance(Request $request, UserRepository $userRepository)
    {
        $user = $this->extractUser($request, $userRepository);
        if (null === $user) {
            return $this->json(['error' => 'invalid api key'], Response::HTTP_UNAUTHORIZED);
        }
        return $this->json(['result' => $user->getDistanceToHome()]);
    }

    /**
     * @Route("/is-in-home-range", name="is-in-home-range")
     * @param Request $request
     * @param UserRepository $userRepository
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getIsInHomeRange(Request $request, UserRepository $userRepository)
    {
        $user = $this->extractUser($request, $userRepository);
        if (null === $user) {
            return $this->json(['error' => 'invalid api key'], Response::HTTP_UNAUTHORIZED);
        }

        return $this->json(['result' => $user->isInHomeRange()]);
    }
}
